# beyondtracks-branding

## Color

- Primary Color: `#582e71`
- Secondary Color: `#543565`

## Font

- [`Montserrat`](https://fonts.google.com/specimen/Montserrat)
